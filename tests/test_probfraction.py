from lea.ext_fraction import ExtFraction as EF
from lea.number import Number
from fractions import Fraction
from decimal import Decimal
import sys
import pytest

def test_ef_is_fraction():
    """Test that a ProbFraction is a Fraction"""
    ef = EF(1,2)
    assert isinstance(ef, EF)
    assert isinstance(ef, Fraction)

def test_ef_as_str():
    ef1 = EF("0")
    assert ef1 == EF(0)
    ef2 = EF("0.5")
    assert ef2 == EF(1,2)
    ef3 = EF("1")
    assert ef3 == EF(1)
    ef4 = EF("0.123456789")
    assert ef4 == EF(123456789,1000000000)
    ef5 = EF("   0.123456789000 ")
    assert ef5 == EF(123456789,1000000000)

def test_ef_from_fraction():
    """Test creating from a fraction"""
    ef = EF._from_fraction(Fraction(3, 4))
    assert ef == EF(3,4)

def test_ef_from_decimal():
    ef1 = EF(Decimal("0"))
    assert ef1 == EF(0)
    ef2 = EF(Decimal("0.5"))
    assert ef2 == EF(1,2)
    ef3 = EF(Decimal("1"))
    assert ef3 == EF(1)
    ef4 = EF(Decimal("0.123456789"))
    assert ef4 == EF(123456789,1000000000)
    ef5 = EF(Decimal("   0.123456789000 "))
    assert ef5 == EF(123456789,1000000000)

def test_ef_from_float():
    """Test creating from a float"""
    # Checking float numbers that can be represented exactly as fractions,
    # i.e. zero and multiples of powers of 2
    ef1 = EF(0.0)
    assert ef1 == EF(0)
    ef2 = EF(1.0)
    assert ef2 == EF(1)
    ef3 = EF(0.5)
    assert ef3 == EF(1,2)
    ef4 = EF(3.0/4.0)
    assert ef4 == EF(3,4)
    # Checking float numbers that cannot be represented exactly as fractions
    # e.g. EF(0.2) is not exactly the same as fraction EF(2,10)
    # we expect no loss of accuracy when converting back the fraction to float
    ef5 = EF(0.2)
    assert float(ef5) == 0.2
    ef6 = EF(2.0/3.0)
    assert float(ef6) == 2.0/3.0
    ef7 = EF(0.123456789)
    assert float(ef7) == 0.123456789
    ef8 = EF(sys.float_info.epsilon)
    assert float(ef8) ==  sys.float_info.epsilon
    ef9 = EF(1.0-sys.float_info.epsilon)
    assert float(ef9) ==  1.0-sys.float_info.epsilon

def test_coerce():
    """Test coercing values to ProbFractions"""
    ef1 = EF(5,6)
    ef2 = EF.coerce(ef1)
    # Coercing a EF does nothing
    assert ef1 is ef2
    f = Fraction(7,8)
    # Coercing a fraction makes it a EF
    ef3 = EF.coerce(f)
    assert isinstance(ef3, EF)
    assert ef3 == f

def test_arithmetic():
    """Check arithmetic operators for correctness (result and type)"""
    ef1 = EF(1,2)
    ef2 = EF(1,3)
    f = Fraction(1,2)
    # Basic arithmetic
    assert ef1+ef2 == EF(5,6)
    assert isinstance(ef1+ef2, EF)
    assert ef1-ef2 == EF(1,6)
    assert isinstance(ef1-ef2, EF)
    assert ef1*ef2 == EF(1,6)
    assert isinstance(ef1*ef2,EF)
    assert ef1/ef2 == EF(3,2)
    assert isinstance(ef1/ef2, EF)
    # Check reversed operators (__radd__ etc)
    assert f+ef2 == EF(5,6)
    assert isinstance(f+ef2, EF)
    assert f-ef2 == EF(1,6)
    assert isinstance(f-ef2, EF)
    assert f*ef2 == EF(1,6)
    assert isinstance(f*ef2,EF)
    assert f/ef2 == EF(3,2)
    assert isinstance(f/ef2, EF)
    # Other operators
    assert +ef1==EF(1,2)
    assert isinstance(+ef1,EF)
    assert -ef1==EF(-1,2)
    assert isinstance(-ef1, EF)
    assert ef1**2==EF(1,4)
    assert isinstance(ef1**2, EF)

def test_check():
    """Test the check method for confirming a fraction is in [0,1]"""
    EF(1,2).check()
    EF(1,1).check()
    EF(0,1).check()
    with pytest.raises(Number.Error):
        EF(2,1).check()
    with pytest.raises(Number.Error):
        EF(-1,1).check()

def test_float_str():
    """Check we can get a float representation of a ProbFraction"""
    assert isinstance(EF(1,2).as_float(), str)
    assert EF(1,2).as_float() == "0.5"

def test_pct_str():
    """Check we can get a percentage representation of a ProbFraction"""
    pct = EF(1,2).as_pct()
    assert isinstance(pct, str)
    # Don't check the exact representation, just that it looks right
    assert pct.endswith('%')
    assert float(pct[:-1]) == 50

def test_string_rep():
    """Test that string representation works"""
    # Depends on the string representation of Fraction
    assert str(EF(1,2)) == str(Fraction(1,2))
