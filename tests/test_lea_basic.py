import lea
from lea.exceptions import LeaError
from lea.ext_fraction import ExtFraction as EF
from math import isclose
from fractions import Fraction
from decimal import Decimal
import pytest
import math
import sys
import os
import tempfile

# All tests are made using fraction representation, in order to ease comparison
@pytest.fixture(scope="module")
def setup():
    lea.set_prob_type('r')

def test_new(setup):
    dist1 = lea.vals(1,2,3,4)
    dist2 = dist1.new()
    assert dist1.equiv(dist2)
    assert dist1 is not dist2

def test_id(setup):
    dist1 = lea.vals(1,2,3,4)
    dist2 = dist1.new()
    assert dist1._id() != dist2._id()
    assert isinstance(dist1._id(), str)

def test_get_leaves_set(setup):
    dist1 = lea.vals(1,2,3,4)
    dist2 = lea.vals(2,4,6,8)
    distcalc1 = (dist1 + dist2) * (dist1 - dist2)
    assert distcalc1.get_leaves_set() == {dist1, dist2}
    dist3 = lea.binom(4,0.2) 
    dist4 = lea.poisson(7)
    distcalc2 = dist3 + dist4 
    assert distcalc2.get_leaves_set() == {dist3, dist4}
    distcalc3 = distcalc1 - distcalc2 
    assert distcalc3.get_leaves_set() == {dist1, dist2, dist3, dist4}

def test_is_dependent_of(setup):
    dist1 = lea.vals(1,2,3,4)
    dist2 = lea.vals(2,4,6,8)
    distcalc1 = (dist1 + dist2) * (dist1 - dist2)
    assert dist1.is_dependent_of(dist1)
    assert distcalc1.is_dependent_of(distcalc1)
    assert distcalc1.is_dependent_of(dist1)
    assert distcalc1.is_dependent_of(dist2)
    assert dist1.is_dependent_of(distcalc1)
    assert dist2.is_dependent_of(distcalc1)
    assert dist1.is_dependent_of(dist2.given(distcalc1==0))
    assert dist1.given(distcalc1==0).is_dependent_of(dist2)
    assert not dist1.is_dependent_of(dist2)
    assert not dist2.is_dependent_of(dist1)
    dist3 = lea.binom(4,0.2)
    dist4 = lea.poisson(7)
    distcalc2 = dist3 + dist4 
    assert dist3.is_dependent_of(dist3)
    assert distcalc2.is_dependent_of(dist3)
    assert dist3.is_dependent_of(distcalc2)
    assert dist4.is_dependent_of(distcalc2)
    assert dist3.is_dependent_of(dist2.given(distcalc2==0))
    assert dist3.given(distcalc2==0).is_dependent_of(dist4)
    assert not distcalc2.is_dependent_of(distcalc1)
    assert not distcalc1.is_dependent_of(distcalc2)
    assert not dist3.is_dependent_of(dist4)
    assert not dist4.is_dependent_of(dist3)
    assert not dist3.is_dependent_of(distcalc1)
    assert not dist4.is_dependent_of(dist1)
    assert not distcalc1.is_dependent_of(dist4)
    assert not distcalc1.is_dependent_of(dist3)
    distcalc3 = distcalc1 - distcalc2 
    assert distcalc3.is_dependent_of(distcalc3)
    assert distcalc3.is_dependent_of(distcalc1)
    assert distcalc3.is_dependent_of(dist1)
    assert distcalc1.is_dependent_of(distcalc3)
    assert dist3.is_dependent_of(distcalc3)
    assert dist4.is_dependent_of(distcalc3)
    assert dist3.is_dependent_of(dist2.given(distcalc3==0))
    assert dist3.given(distcalc3==0).is_dependent_of(dist4)
    assert dist1.given(distcalc3==0).is_dependent_of(dist4)

def test_is_defined_using(setup):
    dist1 = lea.vals(1,2,3,4)
    dist2 = lea.vals(2,4,6,8)
    distcalc1 = (dist1 + dist2) * (dist1 - dist2)
    assert dist1.is_defined_using(dist1)
    assert distcalc1.is_defined_using(distcalc1)
    assert distcalc1.is_defined_using(dist1)
    assert distcalc1.is_defined_using(dist2)
    assert not dist1.is_defined_using(distcalc1)
    assert not dist2.is_defined_using(distcalc1)
    assert not dist1.is_defined_using(dist2.given(distcalc1==0))
    assert dist1.given(distcalc1==0).is_defined_using(dist2)
    assert not dist1.is_defined_using(dist2)
    assert not dist2.is_defined_using(dist1)
    dist3 = lea.binom(4,0.2)
    dist4 = lea.poisson(7)
    distcalc2 = dist3 + dist4
    assert dist3.is_defined_using(dist3)
    assert distcalc2.is_defined_using(dist3)
    assert not dist3.is_defined_using(distcalc2)
    assert not dist4.is_defined_using(distcalc2)
    assert not dist3.is_defined_using(dist2.given(distcalc2==0))
    assert dist3.given(distcalc2==0).is_defined_using(dist4)
    assert not distcalc2.is_defined_using(distcalc1)
    assert not distcalc1.is_defined_using(distcalc2)
    assert not dist3.is_defined_using(dist4)
    assert not dist4.is_defined_using(dist3)
    assert not dist3.is_defined_using(distcalc1)
    assert not dist4.is_defined_using(dist1)
    assert not distcalc1.is_defined_using(dist4)
    assert not distcalc1.is_defined_using(dist3)
    distcalc3 = distcalc1 - distcalc2
    assert distcalc3.is_defined_using(distcalc3)
    assert distcalc3.is_defined_using(distcalc1)
    assert distcalc3.is_defined_using(dist1)
    assert not distcalc1.is_defined_using(distcalc3)
    assert not dist3.is_defined_using(distcalc3)
    assert not dist4.is_defined_using(distcalc3)
    assert not dist3.is_defined_using(dist2.given(distcalc3==0))
    assert dist3.given(distcalc3==0).is_defined_using(dist4)
    assert dist1.given(distcalc3==0).is_defined_using(dist4)

# Constructors
def test_fromvals(setup):
    d = lea.vals(1,2, prob_type='f', ordered=True, sorting=False, normalization=False, check=False)

def test_fromvals_errors(setup):
    # Must be at least one value
    with pytest.raises(LeaError):
        d = lea.vals()
    # No invalid keyword args
    with pytest.raises(TypeError):
        d = lea.vals(1,2, foo=3)
    # Cannot have both ordered and sorting
    with pytest.raises(LeaError):
        d = lea.vals(1,2, ordered=True, sorting=True)
    # Cannot have duplicates with ordered
    with pytest.raises(LeaError):
        d = lea.vals(1,1, ordered=True)
    # Cannot have ordered with dictionary
    with pytest.raises(LeaError):
        d = lea.pmf({1: 2, 2: 5}, ordered=True)

def test_fromvals_ordered(setup):
    d = lea.vals(2,1,3, ordered=True)
    assert d.support() == (2,1,3)
    d = lea.pmf(((2,9),(1,7),(3,5)), ordered=True)
    assert d.support() == (2,1,3)

def test_fromvals_sorting(setup):
    d = lea.vals(2,1,3,2, sorting=True)
    assert d.support() == (1,2,3)

def test_from_dict(setup):
    d = lea.pmf({'a': 5, 'b': 6})
    assert set(d.pmf_tuple()) == {('a', EF(5,11)), ('b', EF(6,11))}

def test_event(setup):
    d = lea.event(0)
    assert d.p(True) == EF(0)
    d = lea.event('1/2')
    assert d.p(True) == EF(1,2)
    d = lea.event(1)
    assert d.p(True) == EF(1)
    d = lea.event(Fraction(123456789,1000000000))
    assert d.p(True) == EF(123456789,1000000000)
    d = lea.event(Decimal("0.123456789"))
    assert d.p(True) == EF(123456789,1000000000)
    d = lea.event(0.0)
    assert d.p(True) == EF(0)
    d = lea.event(0.5)
    assert d.p(True) == EF(1,2)
    d = lea.event(1.0)
    assert d.p(True) == EF(1)
    # the following cases cannot assert exact values due to float representation
    # the pmf method shall convert the prob. fraction back into the given float (no loss of precision)
    d = lea.event(0.2, prob_type='f')
    assert d.p(True) == 0.2
    assert d.p(False) == 1.0 - 0.2
    d = lea.event(0.123456789, prob_type='f')
    assert d.p(True) == 0.123456789
    assert d.p(False) == 1.0 - 0.123456789
    d = lea.event(sys.float_info.epsilon, prob_type='f')
    assert d.p(True) == sys.float_info.epsilon
    assert d.p(False) == 1.0 - sys.float_info.epsilon

def test_bernoulli(setup):
    d = lea.bernoulli(0)
    assert d.p(1) == EF(0)
    d = lea.bernoulli('1/2')
    assert d.p(1) == EF(1,2)
    d = lea.bernoulli(1)
    assert d.p(1) == EF(1)
    d = lea.bernoulli(Fraction(123456789,1000000000))
    assert d.p(1) == EF(123456789,1000000000)
    d = lea.bernoulli(Decimal("0.123456789"))
    assert d.p(1) == EF(123456789,1000000000)
    d = lea.bernoulli(0.0)
    assert d.p(1) == EF(0)
    d = lea.bernoulli(0.5)
    assert d.p(1) == EF(1,2)
    d = lea.bernoulli(1.0)
    assert d.p(1) == EF(1)
    # the following cases cannot assert exact values due to float representation
    # the pmf method shall convert the prob. fraction back into the given float (no loss of precision)
    d = lea.bernoulli(0.2, prob_type='f')
    assert d.p(1) == 0.2
    assert d.p(0) == 1.0 - 0.2
    d = lea.bernoulli(0.123456789, prob_type='f')
    assert d.p(1) == 0.123456789
    assert d.p(0) == 1.0 - 0.123456789
    d = lea.bernoulli(sys.float_info.epsilon, prob_type='f')
    assert d.p(1) == sys.float_info.epsilon
    assert d.p(0) == 1.0 - sys.float_info.epsilon

def test_poisson(setup):
    d = lea.poisson(2)
    # Probability of k events (mean m) is (m**k)*exp(-m)/k!
    expected = 8.0 * math.exp(-2) / 6.0
    # Result is not exact - check it is within 10 decimal places
    assert round(d.p(3)-expected, 10) == 0

def test_is_any_of(setup):
    d1, d2, d3 = lea.interval(1, 6).new(3)
    assert lea.P(d1.is_any_of()) == EF(0, 1)
    assert lea.P(d1.is_any_of(1)) == EF(1, 6)
    assert lea.P(d1.is_any_of(1, 2)) == EF(1, 3)
    assert lea.P(d1.is_any_of(1, 2, 6)) == EF(1, 2)
    assert lea.P(d1.is_any_of(1, 2, 3, 4, 5)) == EF(5, 6)
    assert lea.P(d1.is_any_of(1, 2, 3, 4, 5, 6)) == EF(1, 1)
    assert lea.P(d1.is_any_of(1, 7, 8)) == EF(1, 6)
    assert lea.P(d1.is_any_of(d1)) == EF(1, 1)
    assert lea.P(d1.is_any_of(d2)) == EF(1, 6)
    assert lea.P(d1.is_any_of(d2, d3)) == EF(11, 36)
    assert lea.P(d1.is_any_of(1, d2)) == EF(11, 36)
    assert lea.P(d1.is_any_of(7, d2)) == EF(1, 6)
    assert lea.P(d1.is_any_of(1, d2, d3)) == EF(91, 216)
    assert lea.P(d1.is_any_of(7, d2, d3)) == EF(11, 36)
    assert lea.joint(d1, d2).given(d1.is_any_of(5, d2 - 1))\
           .equiv(lea.vals((1, 2), (2, 3), (3, 4), (4, 5), (5, 1), (5, 2), (5, 3), (5, 4), (5, 5), (5, 6)))

def test_is_none_of(setup):
    d1, d2, d3 = lea.interval(1, 6).new(3)
    assert lea.P(d1.is_none_of()) == EF(1, 1)
    assert lea.P(d1.is_none_of(1)) == EF(5, 6)
    assert lea.P(d1.is_none_of(1, 2)) == EF(2, 3)
    assert lea.P(d1.is_none_of(1, 2, 6)) == EF(1, 2)
    assert lea.P(d1.is_none_of(1, 2, 3, 4, 5)) == EF(1, 6)
    assert lea.P(d1.is_none_of(1, 2, 3, 4, 5, 6)) == EF(0, 1)
    assert lea.P(d1.is_none_of(1, 7, 8)) == EF(5, 6)
    assert lea.P(d1.is_none_of(d1)) == EF(0, 1)
    assert lea.P(d1.is_none_of(d2)) == EF(5, 6)
    assert lea.P(d1.is_none_of(d2, d3)) == EF(25, 36)
    assert lea.P(d1.is_none_of(1, d2)) == EF(25, 36)
    assert lea.P(d1.is_none_of(7, d2)) == EF(5, 6)
    assert lea.P(d1.is_none_of(1, d2, d3)) == EF(125, 216)
    assert lea.P(d1.is_none_of(7, d2, d3)) == EF(25, 36)
    assert lea.joint(d1, d2).given(d1.is_none_of(5, d2 - 1)) \
        .equiv(lea.vals((1, 1), (1, 3), (1, 4), (1, 5), (1, 6), (2, 1), (2, 2), (2, 4), (2, 5),
                        (2, 6), (3, 1), (3, 2), (3, 3), (3, 5), (3, 6), (4, 1), (4, 2), (4, 3),
                        (4, 4), (4, 6), (6, 1), (6, 2), (6, 3), (6, 4), (6, 5), (6, 6)))

def test_given(setup):
    d0 = lea.interval(1, 6)
    d1, d2 = d0.new(2)
    ds = d1 + d2
    assert d1.given().equiv(d0)
    assert d1.given(True).equiv(d0)
    assert d1.given(True, True).equiv(d0)
    assert d1.given(1==1).equiv(d0)
    assert d1.given(1==1, 1!=2).equiv(d0)
    assert d1.given(d1==d1).equiv(d0)
    assert d1.given(d2==d2).equiv(d0)
    assert d1.given(ds==ds).equiv(d0)
    assert d1.given(ds==ds, ds>d1).equiv(d0)
    with pytest.raises(LeaError):
        d1.given(False).calc()
    with pytest.raises(LeaError):
        d1.given(1==0).calc()
    with pytest.raises(LeaError):
        d1.given(d1==0).calc()
    with pytest.raises(LeaError):
        d1.given(d1>6).calc()
    with pytest.raises(LeaError):
        d1.given(d1==d1+1).calc()
    with pytest.raises(LeaError):
        d1.given(d2==d2+1).calc()
    with pytest.raises(LeaError):
        d1.given(d1==d1, ds==ds+1).calc()
    with pytest.raises(LeaError):
        d2.given(d1>=4, ds<=4).calc()
    assert d1.given(d1<=1).equiv(lea.vals(1))
    assert d1.given(d1<=4).equiv(lea.vals(1,2,3,4))
    assert d1.given(d2<=4).equiv(d0)
    assert d1.given(3<=d1, d1<=4).equiv(lea.vals(3,4))
    assert ds.given(3<=ds, ds<=4).equiv(lea.pmf({3: EF(2,5), 4: EF(3,5) }))
    assert ds.given(d1>=3, ds<=4).equiv(lea.vals(4))
    assert d1.given(d1>=3, ds<=4).equiv(lea.vals(3))
    assert d2.given(d1>=3, ds<=4).equiv(lea.vals(1))
    assert ds.given(d1>=3, ds<=5).equiv(lea.pmf({ 4: EF(1,3), 5: EF(2,3) }))
    for v in range(8):
        assert ds.given(d1>=3, ds<=5).p(v) == lea.P((ds==v) & (d1>=3) & (ds<=5)) / lea.P((d1>=3) & (ds<=5))
    assert d1.given(d1>=3, ds<=5).equiv(lea.pmf({ 3: EF(2,3), 4: EF(1,3) }))
    for v in range(8):
        assert d1.given(d1>=3, ds<=5).p(v) == lea.P((d1==v) & (d1>=3) & (ds<=5)) / lea.P((d1>=3) & (ds<=5))
    assert d2.given(d1>=3, ds<=5).equiv(lea.pmf({ 1: EF(2,3), 2: EF(1,3) }))
    for v in range(8):
        assert d2.given(d1>=3, ds<=5).p(v) == lea.P((d2==v) & (d1>=3) & (ds<=5)) / lea.P((d1>=3) & (ds<=5))
    assert (ds.given(ds >= 11).equiv(lea.pmf({ 11: EF(2,3), 12: EF(1, 3) })))
    for v in range(8):
        assert ds.given(ds >= 11).p(v) == lea.P((ds==v) & (ds>=11)) / lea.P(ds>=11)
    assert(lea.joint(d1,d2,ds).given(ds>=11).equiv(lea.pmf({(5, 6, 11): EF(1,3),
                                                            (6, 5, 11): EF(1,3),
                                                            (6, 6, 12): EF(1,3)})))

def test_such_that(setup):
    d0 = lea.interval(1, 6)
    d1, d2 = d0.new(2)
    ds = d1 + d2
    assert d1.such_that().equiv(d0)
    assert d1.such_that(True).equiv(d0)
    assert d1.such_that(True, True).equiv(d0)
    assert d1.such_that(1==1).equiv(d0)
    assert d1.such_that(1==1, 1!=2).equiv(d0)
    assert d1.such_that(d1==d1).equiv(d0)
    assert d1.such_that(d2==d2).equiv(d0)
    with pytest.raises(LeaError):
        assert d1.such_that(ds==ds)
    with pytest.raises(LeaError):
        assert d1.such_that(ds==ds, ds>d1)
    with pytest.raises(LeaError):
        d1.such_that(False).calc()
    with pytest.raises(LeaError):
        d1.such_that(1==0).calc()
    with pytest.raises(LeaError):
        d1.such_that(d1==0).calc()
    with pytest.raises(LeaError):
        d1.such_that(d1>6).calc()
    with pytest.raises(LeaError):
        d1.such_that(d1==d1+1).calc()
    with pytest.raises(LeaError):
        d1.such_that(d2==d2+1).calc()
    with pytest.raises(LeaError):
        d1.such_that(d1==d1, ds==ds+1).calc()
    with pytest.raises(LeaError):
        d2.such_that(d1>=4, ds<=4).calc()
    assert d1.such_that(d1<=1).equiv(lea.vals(1))
    assert d1.such_that(d1<=4).equiv(lea.vals(1,2,3,4))
    assert d1.such_that(d2<=4).equiv(d0)
    assert d1.such_that(3<=d1, d1<=4).equiv(lea.vals(3,4))
    assert ds.such_that(3<=ds, ds<=4).equiv(lea.pmf({3: EF(2,5), 4: EF(3,5) }))
    assert ds.such_that(d1>=3, ds<=4).equiv(lea.vals(4))
    with pytest.raises(LeaError):
        d1.such_that(d1>=3, ds<=4)
    with pytest.raises(LeaError):
        d2.such_that(d1>=3, ds<=4)
    assert ds.such_that(d1>=3, ds<=5).equiv(lea.pmf({ 4: EF(1,4), 5: EF(3,4) }))
    with pytest.raises(LeaError):
        d1.such_that(d1>=3, ds<=5)
    with pytest.raises(LeaError):
        d2.such_that(d1>=3, ds<=5)
    assert (ds.such_that(ds >= 11).equiv(lea.pmf({ 11: EF(2,3), 12: EF(1, 3) })))
    assert(lea.joint(d1,d2,ds).such_that(ds>=11).equiv(lea.pmf({(5, 6, 11): EF(1,3),
                                                                (6, 5, 11): EF(1,3),
                                                                (6, 6, 12): EF(1,3)})))
    # Monty Hall problem #1
    door = "door " + lea.vals(*"ABC")
    prize = door.new()
    choice1 = door.new()
    goat = door.such_that(door != choice1, door != prize)
    choice2a = door.such_that(door == choice1, door != goat)
    choice2b = door.such_that(door != choice1, door != goat)
    assert lea.P(choice2a == prize) == EF(1,3)
    assert lea.P(choice2b == prize) == EF(2,3)
    # Monty Hall problem #2
    with pytest.raises(LeaError):
        door.such_that(door.is_none_of(choice1, prize))
    goat2 = door.such_that(door.is_none_of(choice1, prize), inputs=(choice1, prize))
    choice2a = door.such_that(door == choice1, door != goat2)
    with pytest.raises(LeaError):
        door.such_that(door.is_none_of(choice1, goat2))
    choice2b = door.such_that(door.is_none_of(choice1, goat2), inputs=(choice1, goat2))
    assert lea.P(choice2a == prize) == EF(1,3)
    assert lea.P(choice2b == prize) == EF(2,3)

patients_csv_data = """
Elaine,McLaughlin,female,Ms.,12-01-1984,O+,44.9,141.0,Y
Alan,Music,male,Mr.,30-12-1966,A-,61.3,181.0,Y
Debra,Roberts,female,Mrs.,01-12-1927,O+,79.6,168.0,N
Martine,Albright,female,Mrs.,05-01-1975,O+,46.0,156.0,N
Terry,Jordan,male,Mr.,28-12-1963,A-,96.9,181.0,N
Joshua,Spinney,male,Mr.,19-12-1952,O+,106.4,183.0,N
Tawanna,Short,female,Ms.,02-02-2012,O+,93.4,175.0,N
Frances,Moore,female,Ms.,07-01-1978,B+,91.6,164.0,N
"""[1:]

def test_read_csv_1(setup):
    csv_filename = os.path.join(tempfile.mkdtemp(),"patient.csv")
    with open(csv_filename,'w') as f:
        f.write(patients_csv_data)
    lea.declare_namespace(globals())
    patient = lea.read_csv_file(csv_filename,col_names=('given_name','surname','gender','title','birthday','blood_type','weight{f}','height{f}','smoker{b}'),
                                create_vars=True)
    assert given_name.equiv(patient.given_name)
    assert gender.equiv(patient.gender)
    assert gender.equiv(lea.pmf({"female": 5./8., "male": 3./8.}))
    assert gender.given(smoker).equiv(lea.pmf({"female": 1./2., "male": 1./2.}))
    assert title.equiv(lea.pmf({"Mr.": 3./8., "Mrs.": 2./8., "Ms.": 3./8.}))
    assert smoker.equiv(lea.event(2./8.))
    assert isclose(height.mean(), (141.+181.+168.+156.+181.+183.+175.+164.)/8.)
    assert isclose(height.given(gender=="male").mean(), (181.+181.+183.)/3.)
    
def test_read_csv_2(setup):
    csv_filename = os.path.join(tempfile.mkdtemp(),"patient2.csv")
    with open(csv_filename,'w') as f:
        f.write("given_name2,surname2,gender2,title2,birthday2,blood_type2,weight2{f},height2{f},smoker2\n")
        f.write(patients_csv_data) 
    lea.declare_namespace(globals())
    patient2 = lea.read_csv_file(csv_filename,create_vars=True)
    assert given_name2.equiv(patient2.given_name2)
    assert gender2.equiv(patient2.gender2)
    assert gender2.equiv(lea.pmf({"female": 5./8., "male": 3./8.}))
    assert gender2.given(smoker2=="Y").equiv(lea.pmf({"female": 1./2., "male": 1./2.}))
    assert title2.equiv(lea.pmf({"Mr.": 3./8., "Mrs.": 2./8., "Ms.": 3./8.}))
    assert smoker2.equiv(lea.pmf({"N": 6./8., "Y": 2./8.}))
    assert isclose(height2.mean(), (141.+181.+168.+156.+181.+183.+175.+164.)/8.)
    assert isclose(height2.given(gender2=="male").mean(), (181.+181.+183.)/3.)

def test_read_pandas_dataframe(setup):
    try:
        import pandas as pd
    except:
        pass
    else:
        csv_filename = os.path.join(tempfile.mkdtemp(),"patient2.csv")
        with open(csv_filename,'w') as f:
            f.write("given_name3,surname3,gender3,title3,birthday2,blood_type3,weight3,height3,smoker3\n")
            f.write(patients_csv_data) 
        df = pd.read_csv(csv_filename)
        patient3 = lea.read_pandas_df(df)
        # warning: unlike Lea,pandas make an automatic conversion of float-looking fields into float
        #          that's why we add {f} formatters below
        height3 = patient3.height3
        gender3 = patient3.gender3
        assert isclose(height3.given(gender3=="male").mean(), (181.+181.+183.)/3.)
        with open(csv_filename,'w') as f:
            f.write("given_name3,surname3,gender3,title3,birthday2,blood_type3,weight3{f},height3{f},smoker3\n")
            f.write(patients_csv_data)         
        expected_patient3 = lea.read_csv_file(csv_filename)
        assert patient3.equiv(expected_patient3)

def test_bn_from_joint(setup):
    csv_filename = os.path.join(tempfile.mkdtemp(),"patient.csv")
    with open(csv_filename,'w') as f:
        f.write("given_name,surname,gender,title,birthday,blood_type,weight{f},height{f},smoker\n")
        f.write(patients_csv_data)
    patient = lea.read_csv_file(csv_filename)
    bn = patient.build_bn_from_joint(
           ( ('gender','blood_type')     , 'smoker' ),
           ( ('gender','height','smoker'), 'weight' ))
    assert bn.gender.equiv(lea.pmf({"female": 5./8., "male": 3./8.}))
    assert bn.gender.given(80 <= bn.weight, bn.weight < 100).equiv_f(lea.pmf({"female": 0.603011437671927,
                                                                              "male"  : 0.39698856232807295}))

def test_draw_unsorted_without_replacement(setup):
    # test an unbiased die
    d = lea.interval(1,6)
    d0 = d.draw(0)
    assert d0.equiv(lea.vals(()))
    d1 = d.draw(1)
    assert d1.equiv(d.map(lambda v: (v,)))
    d2 = d.draw(2)
    assert len(d2._vs) == 6*5
    assert d2.p((1,1)) == 0
    assert d2.p((6,6)) == 0
    assert d2.p((1,2)) == EF(1,6) * EF(1,5)
    assert d2.p((2,1)) == EF(1,6) * EF(1,5)
    assert d2.p((2,3)) == EF(1,6) * EF(1,5)
    d3 = d.draw(3)
    assert len(d3._vs) == 6*5*4
    assert d3.p((1,2,1)) == 0
    assert d3.p((1,6,6)) == 0
    assert d3.p((1,2,3)) == EF(1,6) * EF(1,5) * EF(1,4)
    assert d3.p((2,1,3)) == EF(1,6) * EF(1,5) * EF(1,4)
    assert d3.p((2,3,1)) == EF(1,6) * EF(1,5) * EF(1,4)
    assert d3.p((2,3,4)) == EF(1,6) * EF(1,5) * EF(1,4)
    assert d3.equiv(d.draw(3,sorted=False,replacement=False))
    with pytest.raises(LeaError):
        d7 = d.draw(7)
    # test a biased die, with P(d==1) = 2/7
    d = lea.pmf({1: 2, 2: 1, 3: 1, 4: 1, 5: 1, 6: 1})
    d0 = d.draw(0)
    assert d0.equiv(lea.vals(()))
    d1 = d.draw(1)
    assert d1.equiv(d.map(lambda v: (v,)))
    d2 = d.draw(2)
    assert len(d2._vs) == 6*5
    assert d2.p((1,1)) == 0
    assert d2.p((6,6)) == 0
    assert d2.p((1,2)) == EF(2,7) * EF(1,5)
    assert d2.p((2,1)) == EF(1,7) * EF(2,6)
    assert d2.p((2,3)) == EF(1,7) * EF(1,6)
    d3 = d.draw(3)
    assert len(d3._vs) == 6*5*4
    assert d3.p((1,2,1)) == 0
    assert d3.p((1,6,6)) == 0
    assert d3.p((1,2,3)) == EF(2,7) * EF(1,5) * EF(1,4)
    assert d3.p((2,1,3)) == EF(1,7) * EF(2,6) * EF(1,4)
    assert d3.p((2,3,1)) == EF(1,7) * EF(1,6) * EF(2,5)
    assert d3.p((2,3,4)) == EF(1,7) * EF(1,6) * EF(1,5)
    assert d3.equiv(d.draw(3,sorted=False,replacement=False))
    with pytest.raises(LeaError):
        d7 = d.draw(7)

# TODO LOOP
def test_draw_unsorted_with_replacement(setup):
    # test an unbiased die
    d = lea.interval(1,6)
    d0 = d.draw(0,replacement=True)
    assert d0.equiv(lea.vals(()))
    d1 = d.draw(1,replacement=True)
    assert d1.equiv(d.map(lambda v: (v,)))
    d2 = d.draw(2,replacement=True)
    assert len(d2._vs) == 6**2
    assert d2.p((1,1)) == EF(1,6) * EF(1,6)
    assert d2.p((6,6)) == EF(1,6) * EF(1,6)
    assert d2.p((1,2)) == EF(1,6) * EF(1,6)
    assert d2.p((2,1)) == EF(1,6) * EF(1,6)
    assert d2.p((2,3)) == EF(1,6) * EF(1,6)
    d3 = d.draw(3,replacement=True)
    assert len(d3._vs) == 6**3
    assert d3.p((1,2,1)) == EF(1,6) * EF(1,6) * EF(1,6)
    assert d3.p((1,6,6)) == EF(1,6) * EF(1,6) * EF(1,6)
    assert d3.p((1,2,3)) == EF(1,6) * EF(1,6) * EF(1,6)
    assert d3.p((2,1,3)) == EF(1,6) * EF(1,6) * EF(1,6)
    assert d3.p((2,3,1)) == EF(1,6) * EF(1,6) * EF(1,6)
    assert d3.p((2,3,4)) == EF(1,6) * EF(1,6) * EF(1,6)
    assert d3.equiv(d.draw(3,sorted=False,replacement=True))
    d4 = d.draw(4,replacement=True)
    assert len(d4._vs) == 6**4
    # test a biased die, with P(d==1) = 2/7
    d = lea.pmf({1: 2, 2: 1, 3: 1, 4: 1, 5: 1, 6: 1})
    d0 = d.draw(0,replacement=True)
    assert d0.equiv(lea.vals(()))
    d1 = d.draw(1,replacement=True)
    assert d1.equiv(d.map(lambda v: (v,)))
    d2 = d.draw(2,replacement=True)
    assert len(d2._vs) == 6**2
    assert d2.p((1,1)) == EF(2,7) * EF(2,7)
    assert d2.p((6,6)) == EF(1,7) * EF(1,7)
    assert d2.p((1,2)) == EF(2,7) * EF(1,7)
    assert d2.p((2,1)) == EF(1,7) * EF(2,7)
    assert d2.p((2,3)) == EF(1,7) * EF(1,7)
    d3 = d.draw(3,replacement=True)
    assert len(d3._vs) == 6**3
    assert d3.p((1,2,1)) == EF(2,7) * EF(2,7) * EF(1,7)
    assert d3.p((1,6,6)) == EF(2,7) * EF(1,7) * EF(1,7)
    assert d3.p((1,2,3)) == EF(2,7) * EF(1,7) * EF(1,7)
    assert d3.p((2,1,3)) == EF(1,7) * EF(2,7) * EF(1,7)
    assert d3.p((2,3,1)) == EF(1,7) * EF(1,7) * EF(2,7)
    assert d3.p((2,3,4)) == EF(1,7) * EF(1,7) * EF(1,7)
    assert d3.equiv(d.draw(3,sorted=False,replacement=True))
    d4 = d.draw(4,replacement=True)
    assert len(d4._vs) == 6**4

def test_draw_sorted_without_replacement(setup):
    # test an unbiased die
    d = lea.interval(1,6)
    d0 = d.draw(0,sorted=True)
    assert d0.equiv(lea.vals(()))
    d1 = d.draw(1,sorted=True)
    assert d1.equiv(d.map(lambda v: (v,)))
    d2 = d.draw(2,sorted=True)
    assert len(d2._vs) == 15
    assert d2.p((1,1)) == 0
    assert d2.p((6,6)) == 0
    assert d2.p((1,2)) == 2 * EF(1,6) * EF(1,5)
    assert d2.p((2,1)) == 0
    assert d2.p((2,3)) == 2 * EF(1,6) * EF(1,5)
    assert d2.equiv(d.draw(2).map(lambda vs: tuple(sorted(vs))).get_alea())
    d3 = d.draw(3,sorted=True)
    assert len(d3._vs) == 20
    assert d3.p((1,2,1)) == 0
    assert d3.p((1,6,6)) == 0
    assert d3.p((1,2,3)) == 6 * EF(1,6) * EF(1,5) * EF(1,4)
    assert d3.p((2,1,3)) == 0
    assert d3.p((2,3,1)) == 0
    assert d3.p((2,3,4)) == 6 * EF(1,6) * EF(1,5) * EF(1,4)
    assert d3.equiv(d.draw(3,sorted=True,replacement=False))
    assert d3.equiv(d.draw(3).map(lambda vs: tuple(sorted(vs))).get_alea())
    with pytest.raises(LeaError):
        d7 = d.draw(7,sorted=True)
    # test a biased die, with P(d==1) = 2/7
    d = lea.pmf({1: 2, 2: 1, 3: 1, 4: 1, 5: 1, 6: 1})
    d0 = d.draw(0,sorted=True)
    assert d0.equiv(lea.vals(()))
    d1 = d.draw(1,sorted=True)
    assert d1.equiv(d.map(lambda v: (v,)))
    d2 = d.draw(2,sorted=True)
    assert len(d2._vs) == 15
    assert d2.p((1,1)) == 0
    assert d2.p((6,6)) == 0
    assert d2.p((1,2)) == EF(2,7) * EF(1,5) + EF(1,7) * EF(2,6)
    assert d2.p((2,1)) == 0
    assert d2.p((2,3)) == EF(1,7) * EF(1,6) + EF(1,7) * EF(1,6)
    assert d2.equiv(d.draw(2).map(lambda vs: tuple(sorted(vs))).get_alea())
    d3 = d.draw(3,sorted=True)
    assert len(d3._vs) == 20
    assert d3.p((1,2,1)) == 0
    assert d3.p((1,6,6)) == 0
    assert d3.p((1,2,3)) == 2 * (EF(2,7) * EF(1,5) * EF(1,4) + EF(1,7) * EF(1,6) * EF(2,5) + EF(1,7) * EF(2,6) * EF(1,4))
    assert d3.p((2,1,3)) == 0
    assert d3.p((2,3,1)) == 0
    assert d3.p((2,3,4)) == 6 * EF(1,7) * EF(1,6) * EF(1,5)
    assert d3.equiv(d.draw(3,sorted=True,replacement=False))
    assert d3.equiv(d.draw(3).map(lambda vs: tuple(sorted(vs))).get_alea())
    with pytest.raises(LeaError):
        d7 = d.draw(7,sorted=True)

def test_draw_sorted_with_replacement(setup):
    # test an unbiased die
    d = lea.interval(1,6)
    d0 = d.draw(0,sorted=True,replacement=True)
    assert d0.equiv(lea.vals(()))
    d1 = d.draw(1,sorted=True,replacement=True)
    assert d1.equiv(d.map(lambda v: (v,)))
    d2 = d.draw(2,sorted=True,replacement=True)
    assert len(d2._vs) == 21
    assert d2.p((1,1)) == EF(1,6) * EF(1,6)
    assert d2.p((6,6)) == EF(1,6) * EF(1,6)
    assert d2.p((1,2)) == 2 * EF(1,6) * EF(1,6)
    assert d2.p((2,1)) == 0
    assert d2.p((2,3)) == 2 * EF(1,6) * EF(1,6)
    assert d2.equiv(d.draw(2,replacement=True).map(lambda vs: tuple(sorted(vs))).get_alea())
    d3 = d.draw(3,sorted=True,replacement=True)
    assert len(d3._vs) == 56
    assert d3.p((1,2,1)) == 0
    assert d3.p((1,6,6)) == 3 * EF(1,6) * EF(1,6) * EF(1,6)
    assert d3.p((1,2,3)) == 6 * EF(1,6) * EF(1,6) * EF(1,6)
    assert d3.p((2,1,3)) == 0
    assert d3.p((2,3,1)) == 0
    assert d3.p((2,3,4)) == 6 * EF(1,6) * EF(1,6) * EF(1,6)
    assert d3.equiv(d.draw(3,replacement=True).map(lambda vs: tuple(sorted(vs))).get_alea())
    d7 = d.draw(7,sorted=True,replacement=True)
    assert len(d7._vs) == 792
    # test a biased die, with P(d==1) = 2/7
    d = lea.pmf({1: 2, 2: 1, 3: 1, 4: 1, 5: 1, 6: 1})
    d0 = d.draw(0,sorted=True,replacement=True)
    assert d0.equiv(lea.vals(()))
    d1 = d.draw(1,sorted=True,replacement=True)
    assert d1.equiv(d.map(lambda v: (v,)))
    d2 = d.draw(2,sorted=True,replacement=True)
    assert len(d2._vs) == 21
    assert d2.p((1,1)) == EF(2,7) * EF(2,7)
    assert d2.p((6,6)) == EF(1,7) * EF(1,7)
    assert d2.p((1,2)) == 2 * EF(2,7) * EF(1,7)
    assert d2.p((2,1)) == 0
    assert d2.p((2,3)) == 2 * EF(1,7) * EF(1,7)
    assert d2.equiv(d.draw(2,replacement=True).map(lambda vs: tuple(sorted(vs))).get_alea())
    d3 = d.draw(3,sorted=True,replacement=True)
    assert len(d3._vs) == 56
    assert d3.p((1,2,1)) == 0
    assert d3.p((1,6,6)) == 3 * EF(2,7) * EF(1,7) * EF(1,7)
    assert d3.p((1,2,3)) == 6 * EF(2,7) * EF(1,7) * EF(1,7)
    assert d3.p((2,1,3)) == 0
    assert d3.p((2,3,1)) == 0
    assert d3.p((2,3,4)) == 6 * EF(1,7) * EF(1,7) * EF(1,7)
    assert d3.equiv(d.draw(3,replacement=True).map(lambda vs: tuple(sorted(vs))).get_alea())
    d7 = d.draw(7,sorted=True,replacement=True)
    assert len(d7._vs) == 792

def test_given_prob(setup):
    die = lea.interval(1, 6, prob_type='r')
    assert die.given_prob(p=1).equiv(die)
    assert die.given_prob(True, p=1).equiv(die)
    assert die.given_prob(False, p=0).equiv(die)
    assert die.given_prob(die<=die, p=1).equiv(die)
    with pytest.raises(LeaError):
        die.given_prob(p='1/2')
    with pytest.raises(LeaError):
        die.given_prob(True, p='1/2')
    with pytest.raises(LeaError):
        die.given_prob(False, p='1/2')
    with pytest.raises(LeaError):
        die.given_prob(p=0)
    with pytest.raises(LeaError):
        die.given_prob(True, p=0)
    with pytest.raises(LeaError):
        die.given_prob(False, p=1)
    revised_die1 = die.given_prob(die >= 5, p='1/2')
    assert revised_die1.equiv(lea.pmf({1: EF(1,8), 2: EF(1,8), 3: EF(1,8),
                                       4: EF(1,8), 5: EF(1,4), 6: EF(1,4)}))
    revised_die2 = die.given_prob(2 <= die, die <= 4, p='1/4')
    assert revised_die2.equiv(lea.pmf({1: EF(1,4),  2: EF(1,12), 3: EF(1,12),
                                       4: EF(1,12), 5: EF(1,4),  6: EF(1,4)}))
    revised_die3 = die.given_prob(die.is_any_of(1,5,6), p='3/4')
    assert revised_die3.equiv(revised_die2)
    revised_die4 = die.given_prob(2 <= die, die <= 4, p=1)
    assert revised_die4.equiv(die.given(2 <= die, die <= 4))
    revised_die5 = die.given_prob(2 <= die, die <= 4, p=0)
    assert revised_die5.equiv(die.given(lea.any_false(2 <= die, die <= 4)))

def test_iter_alea(setup):
    with pytest.raises(LeaError):
        len(lea.vals(1))
    with pytest.raises(LeaError):
        tuple(lea.vals(1))
    with pytest.raises(LeaError):
        len(lea.vals(1, 2, 3))
    with pytest.raises(LeaError):
        tuple(lea.vals(1, 2, 3))
    with pytest.raises(LeaError):
        len(lea.vals("123", "12", "123"))
    with pytest.raises(LeaError):
        tuple(lea.vals("123", "12", "123"))
    with pytest.raises(LeaError):
        len(lea.vals(*"123")+"")
    with pytest.raises(LeaError):
        tuple(lea.vals(*"123")+"")
    with pytest.raises(LeaError):
        len(lea.vals(*"123").given(True))
    with pytest.raises(LeaError):
        tuple(lea.vals(*"123").given(True))
    dt0 = lea.vals(*"ABC", prob_type='r')
    assert len(dt0) == 1
    dt = tuple(dt0)
    assert len(dt) == 1
    assert dt[0].equiv(dt0)
    assert (dt[0] == dt0).is_certain()
    dt01 = lea.vals("AB", "CD", "ED")
    assert len(dt01) == 2
    dt = tuple(dt01)
    assert len(dt) == 2
    assert dt[0].equiv(lea.vals(*"ACE", prob_type='r'))
    assert dt[1].equiv(lea.vals(*"BDD", prob_type='r'))

def test_iter_clea(setup):
    d0 = lea.vals(*"ACE")
    d = lea.joint(d0)
    assert len(d) == 1
    dt = tuple(d)
    assert len(dt) == 1
    assert (dt[0] == d0).is_certain()
    assert (lea.joint(*dt) == d).is_certain()
    d1 = lea.vals(*"BDD")
    d = lea.joint(d0, d1)
    assert len(d) == 2
    dt = tuple(d)
    assert len(dt) == 2
    assert (dt[0] == d0).is_certain()
    assert (dt[1] == d1).is_certain()
    assert (lea.joint(*dt) == d).is_certain()
    d2 = lea.vals(*"XY")
    d = lea.joint(d0, d1, d2)
    assert len(d) == 3
    dt = tuple(d)
    assert len(dt) == 3
    assert (dt[0] == d0).is_certain()
    assert (dt[1] == d1).is_certain()
    assert (dt[2] == d2).is_certain()
    assert (lea.joint(*dt) == d).is_certain()
